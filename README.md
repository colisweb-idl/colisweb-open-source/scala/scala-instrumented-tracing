# scala-instrumented-tracing

Utilities to use tracing alongside a comprehensive instrumentation, like when using Datadog's java-agent.

## scala-instrumented-tracing-annotations

A `@Trace` annotation without dependencies on other libraries to avoid dependency clash between compile-time dependencies and an instrumentation agant.

Example : when using Datadog's java agent