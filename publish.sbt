ReleaseSettings.globalReleaseSettings
ReleaseSettings.buildReleaseSettings(
  "Utilities to use tracing alongside a comprehensive instrumentation, like when using Datadog's java-agent.",
  "MIT",
  "http://opensource.org/licenses/MIT",
  "scala-instrumented-tracing"
)

ThisBuild / developers := List(Developers.cyrilVerdier)
